from ast import main
import opcode
from sys import argv
import struct

if len(argv) != 2:
    print(f"usage: python3 {argv[0]} <input-file>")
    exit(1)

fname = argv[-1]

VKD3D_SM1_VS = 0xfffe
VKD3D_SM1_PS = 0xffff

vkd3d_sm1_opcode = {
    0x00 : "VKD3D_SM1_OP_NOP",
    0x01 : "VKD3D_SM1_OP_MOV",
    0x02 : "VKD3D_SM1_OP_ADD",
    0x03 : "VKD3D_SM1_OP_SUB",
    0x04 : "VKD3D_SM1_OP_MAD",
    0x05 : "VKD3D_SM1_OP_MUL",
    0x06 : "VKD3D_SM1_OP_RCP",
    0x07 : "VKD3D_SM1_OP_RSQ",
    0x08 : "VKD3D_SM1_OP_DP3",
    0x09 : "VKD3D_SM1_OP_DP4",
    0x0a : "VKD3D_SM1_OP_MIN",
    0x0b : "VKD3D_SM1_OP_MAX",
    0x0c : "VKD3D_SM1_OP_SLT",
    0x0d : "VKD3D_SM1_OP_SGE",
    0x0e : "VKD3D_SM1_OP_EXP",
    0x0f : "VKD3D_SM1_OP_LOG",
    0x10 : "VKD3D_SM1_OP_LIT",
    0x11 : "VKD3D_SM1_OP_DST",
    0x12 : "VKD3D_SM1_OP_LRP",
    0x13 : "VKD3D_SM1_OP_FRC",
    0x14 : "VKD3D_SM1_OP_M4x4",
    0x15 : "VKD3D_SM1_OP_M4x3",
    0x16 : "VKD3D_SM1_OP_M3x4",
    0x17 : "VKD3D_SM1_OP_M3x3",
    0x18 : "VKD3D_SM1_OP_M3x2",
    0x19 : "VKD3D_SM1_OP_CALL",
    0x1a : "VKD3D_SM1_OP_CALLNZ",
    0x1b : "VKD3D_SM1_OP_LOOP",
    0x1c : "VKD3D_SM1_OP_RET",
    0x1d : "VKD3D_SM1_OP_ENDLOOP",
    0x1e : "VKD3D_SM1_OP_LABEL",
    0x1f : "VKD3D_SM1_OP_DCL",
    0x20 : "VKD3D_SM1_OP_POW",
    0x21 : "VKD3D_SM1_OP_CRS",
    0x22 : "VKD3D_SM1_OP_SGN",
    0x23 : "VKD3D_SM1_OP_ABS",
    0x24 : "VKD3D_SM1_OP_NRM",
    0x25 : "VKD3D_SM1_OP_SINCOS",
    0x26 : "VKD3D_SM1_OP_REP",
    0x27 : "VKD3D_SM1_OP_ENDREP",
    0x28 : "VKD3D_SM1_OP_IF",
    0x29 : "VKD3D_SM1_OP_IFC",
    0x2a : "VKD3D_SM1_OP_ELSE",
    0x2b : "VKD3D_SM1_OP_ENDIF",
    0x2c : "VKD3D_SM1_OP_BREAK",
    0x2d : "VKD3D_SM1_OP_BREAKC",
    0x2e : "VKD3D_SM1_OP_MOVA",
    0x2f : "VKD3D_SM1_OP_DEFB",
    0x30 : "VKD3D_SM1_OP_DEFI",

    0x40 : "VKD3D_SM1_OP_TEXCOORD",
    0x41 : "VKD3D_SM1_OP_TEXKILL",
    0x42 : "VKD3D_SM1_OP_TEX",
    0x43 : "VKD3D_SM1_OP_TEXBEM",
    0x44 : "VKD3D_SM1_OP_TEXBEML",
    0x45 : "VKD3D_SM1_OP_TEXREG2AR",
    0x46 : "VKD3D_SM1_OP_TEXREG2GB",
    0x47 : "VKD3D_SM1_OP_TEXM3x2PAD",
    0x48 : "VKD3D_SM1_OP_TEXM3x2TEX",
    0x49 : "VKD3D_SM1_OP_TEXM3x3PAD",
    0x4a : "VKD3D_SM1_OP_TEXM3x3TEX",
    0x4b : "VKD3D_SM1_OP_TEXM3x3DIFF",
    0x4c : "VKD3D_SM1_OP_TEXM3x3SPEC",
    0x4d : "VKD3D_SM1_OP_TEXM3x3VSPEC",
    0x4e : "VKD3D_SM1_OP_EXPP",
    0x4f : "VKD3D_SM1_OP_LOGP",
    0x50 : "VKD3D_SM1_OP_CND",
    0x51 : "VKD3D_SM1_OP_DEF",
    0x52 : "VKD3D_SM1_OP_TEXREG2RGB",
    0x53 : "VKD3D_SM1_OP_TEXDP3TEX",
    0x54 : "VKD3D_SM1_OP_TEXM3x2DEPTH",
    0x55 : "VKD3D_SM1_OP_TEXDP3",
    0x56 : "VKD3D_SM1_OP_TEXM3x3",
    0x57 : "VKD3D_SM1_OP_TEXDEPTH",
    0x58 : "VKD3D_SM1_OP_CMP",
    0x59 : "VKD3D_SM1_OP_BEM",
    0x5a : "VKD3D_SM1_OP_DP2ADD",
    0x5b : "VKD3D_SM1_OP_DSX",
    0x5c : "VKD3D_SM1_OP_DSY",
    0x5d : "VKD3D_SM1_OP_TEXLDD",
    0x5e : "VKD3D_SM1_OP_SETP",
    0x5f : "VKD3D_SM1_OP_TEXLDL",
    0x60 : "VKD3D_SM1_OP_BREAKP",

    0xfffd : "VKD3D_SM1_OP_PHASE",
    0xfffe : "VKD3D_SM1_OP_COMMENT",
    0Xffff : "VKD3D_SM1_OP_END",
};

vs_opcode_table = [
    # Arithmetic
    ("VKD3D_SM1_OP_NOP" ,          (0, 0) ),
    ("VKD3D_SM1_OP_MOV" ,          (1, 1) ),
    ("VKD3D_SM1_OP_MOVA" ,         (1, 1, (2, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_ADD" ,          (1, 2) ),
    ("VKD3D_SM1_OP_SUB" ,          (1, 2) ),
    ("VKD3D_SM1_OP_MAD" ,          (1, 3) ),
    ("VKD3D_SM1_OP_MUL" ,          (1, 2) ),
    ("VKD3D_SM1_OP_RCP" ,          (1, 1) ),
    ("VKD3D_SM1_OP_RSQ" ,          (1, 1) ),
    ("VKD3D_SM1_OP_DP3" ,          (1, 2) ),
    ("VKD3D_SM1_OP_DP4" ,          (1, 2) ),
    ("VKD3D_SM1_OP_MIN" ,          (1, 2) ),
    ("VKD3D_SM1_OP_MAX" ,          (1, 2) ),
    ("VKD3D_SM1_OP_SLT" ,          (1, 2) ),
    ("VKD3D_SM1_OP_SGE" ,          (1, 2) ),
    ("VKD3D_SM1_OP_ABS" ,          (1, 1) ),
    ("VKD3D_SM1_OP_EXP" ,          (1, 1) ),
    ("VKD3D_SM1_OP_LOG" ,          (1, 1) ),
    ("VKD3D_SM1_OP_EXPP" ,         (1, 1) ),
    ("VKD3D_SM1_OP_LOGP" ,         (1, 1) ),
    ("VKD3D_SM1_OP_LIT" ,          (1, 1) ),
    ("VKD3D_SM1_OP_DST" ,          (1, 2) ),
    ("VKD3D_SM1_OP_LRP" ,          (1, 3) ),
    ("VKD3D_SM1_OP_FRC" ,          (1, 1) ),
    ("VKD3D_SM1_OP_POW" ,          (1, 2) ),
    ("VKD3D_SM1_OP_CRS" ,          (1, 2) ),
    ("VKD3D_SM1_OP_SGN" ,          (1, 3, (2, 0), (  2,   1)) ),
    ("VKD3D_SM1_OP_SGN" ,          (1, 1, (3, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_NRM" ,          (1, 1,) ),
    ("VKD3D_SM1_OP_SINCOS" ,       (1, 3, (2, 0), (  2,   1)) ),
    ("VKD3D_SM1_OP_SINCOS" ,       (1, 1, (3, 0), (0xffffffff, 0xffffffff)) ),
    # Matrix
    ("VKD3D_SM1_OP_M4x4" ,         (1, 2) ),
    ("VKD3D_SM1_OP_M4x3" ,         (1, 2) ),
    ("VKD3D_SM1_OP_M3x4" ,         (1, 2) ),
    ("VKD3D_SM1_OP_M3x3" ,         (1, 2) ),
    ("VKD3D_SM1_OP_M3x2" ,         (1, 2) ),
    # Declarations
    ("VKD3D_SM1_OP_DCL" ,          (0, 2) ),
    # Constant definitions
    ("VKD3D_SM1_OP_DEF" ,          (1, 4) ),
    ("VKD3D_SM1_OP_DEFB" ,         (1, 1) ),
    ("VKD3D_SM1_OP_DEFI" ,         (1, 4) ),
    # Control flow
    ("VKD3D_SM1_OP_REP" ,          (0, 1, (2, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_ENDREP" ,       (0, 0, (2, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_IF" ,           (0, 1, (2, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_IFC" ,          (0, 2, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_ELSE" ,         (0, 0, (2, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_ENDIF" ,        (0, 0, (2, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_BREAK" ,        (0, 0, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_BREAKC" ,       (0, 2, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_BREAKP" ,       (0, 1) ),
    ("VKD3D_SM1_OP_CALL" ,         (0, 1, (2, 0), {0xffffffff, 0xffffffff}) ),
    ("VKD3D_SM1_OP_CALLNZ" ,       (0, 2, (2, 0), {0xffffffff, 0xffffffff}) ),
    ("VKD3D_SM1_OP_LOOP" ,         (0, 2, (2, 0), {0xffffffff, 0xffffffff}) ),
    ("VKD3D_SM1_OP_RET" ,          (0, 0, (2, 0), {0xffffffff, 0xffffffff}) ),
    ("VKD3D_SM1_OP_ENDLOOP" ,      (0, 0, (2, 0), {0xffffffff, 0xffffffff}) ),
    ("VKD3D_SM1_OP_LABEL" ,        (0, 1, (2, 0), {0xffffffff, 0xffffffff}) ),

    ("VKD3D_SM1_OP_SETP" ,         (1, 2) ),
    ("VKD3D_SM1_OP_TEXLDL" ,       (1, 2, (3, 0), (0xffffffff, 0xffffffff)) ),
]

ps_opcode_table = [
    # Arithmetic
    ("VKD3D_SM1_OP_NOP" ,          (0, 0) ),
    ("VKD3D_SM1_OP_MOV" ,          (1, 1) ),
    ("VKD3D_SM1_OP_ADD" ,          (1, 2) ),
    ("VKD3D_SM1_OP_SUB" ,          (1, 2) ),
    ("VKD3D_SM1_OP_MAD" ,          (1, 3) ),
    ("VKD3D_SM1_OP_MUL" ,          (1, 2) ),
    ("VKD3D_SM1_OP_RCP" ,          (1, 1) ),
    ("VKD3D_SM1_OP_RSQ" ,          (1, 1) ),
    ("VKD3D_SM1_OP_DP3" ,          (1, 2) ),
    ("VKD3D_SM1_OP_DP4" ,          (1, 2) ),
    ("VKD3D_SM1_OP_MIN" ,          (1, 2) ),
    ("VKD3D_SM1_OP_MAX" ,          (1, 2) ),
    ("VKD3D_SM1_OP_SLT" ,          (1, 2) ),
    ("VKD3D_SM1_OP_SGE" ,          (1, 2) ),
    ("VKD3D_SM1_OP_ABS" ,          (1, 1) ),
    ("VKD3D_SM1_OP_EXP" ,          (1, 1) ),
    ("VKD3D_SM1_OP_LOG" ,          (1, 1) ),
    ("VKD3D_SM1_OP_EXPP" ,         (1, 1) ),
    ("VKD3D_SM1_OP_LOGP" ,         (1, 1) ),
    ("VKD3D_SM1_OP_DST" ,          (1, 2) ),
    ("VKD3D_SM1_OP_LRP" ,          (1, 3) ),
    ("VKD3D_SM1_OP_FRC" ,          (1, 1) ),
    ("VKD3D_SM1_OP_CND" ,          (1, 3, (1, 0), (  1,   4)) ),
    ("VKD3D_SM1_OP_CMP" ,          (1, 3, (1, 2), (  3,   0)) ),
    ("VKD3D_SM1_OP_POW" ,          (1, 2) ),
    ("VKD3D_SM1_OP_CRS" ,          (1, 2) ),
    ("VKD3D_SM1_OP_NRM" ,          (1, 1) ),
    ("VKD3D_SM1_OP_SINCOS" ,       (1, 3, (2, 0), (  2,   1)) ),
    ("VKD3D_SM1_OP_SINCOS" ,       (1, 1, (3, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_DP2ADD" ,       (1, 3, (2, 0), (0xffffffff, 0xffffffff)) ),
    # Matrix
    ("VKD3D_SM1_OP_M4x4" ,         (1, 2) ),
    ("VKD3D_SM1_OP_M4x3" ,         (1, 2) ),
    ("VKD3D_SM1_OP_M3x4" ,         (1, 2) ),
    ("VKD3D_SM1_OP_M3x3" ,         (1, 2) ),
    ("VKD3D_SM1_OP_M3x2" ,         (1, 2) ),
    # Declarations
    ("VKD3D_SM1_OP_DCL" ,          (0, 2) ),
    # Constant definitions
    ("VKD3D_SM1_OP_DEF" ,          (1, 4) ),
    ("VKD3D_SM1_OP_DEFB" ,         (1, 1) ),
    ("VKD3D_SM1_OP_DEFI" ,         (1, 4) ),
    # Control flow
    ("VKD3D_SM1_OP_REP" ,          (0, 1, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_ENDREP" ,       (0, 0, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_IF" ,           (0, 1, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_IFC" ,          (0, 2, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_ELSE" ,         (0, 0, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_ENDIF" ,        (0, 0, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_BREAK" ,        (0, 0, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_BREAKC" ,       (0, 2, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_BREAKP" ,       (0, 1) ),
    ("VKD3D_SM1_OP_CALL" ,         (0, 1, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_CALLNZ" ,       (0, 2, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_LOOP" ,         (0, 2, (3, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_RET" ,          (0, 0, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_ENDLOOP" ,      (0, 0, (3, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_LABEL" ,        (0, 1, (2, 1), (0xffffffff, 0xffffffff)) ),
    # Texture
    ("VKD3D_SM1_OP_TEXCOORD" ,     (1, 0, (0, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXCOORD" ,     (1, 1, (1 ,4), (  1,   4)) ),
    ("VKD3D_SM1_OP_TEXKILL" ,      (1, 0, (1 ,0), (  3,   0)) ),
    ("VKD3D_SM1_OP_TEX" ,          (1, 0, (0, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEX" ,          (1, 1, (1, 4), (  1,   4)) ),
    ("VKD3D_SM1_OP_TEX" ,          (1, 2, (2, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_TEXBEM" ,       (1, 1, (0, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXBEML" ,      (1, 1, (1, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXREG2AR" ,    (1, 1, (1, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXREG2GB" ,    (1, 1, (1, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXREG2RGB" ,   (1, 1, (1, 2), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXM3x2PAD" ,   (1, 1, (1, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXM3x2TEX" ,   (1, 1, (1, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXM3x3PAD" ,   (1, 1, (1, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXM3x3DIFF" ,  (1, 1, (0, 0), (  0,   0)) ),
    ("VKD3D_SM1_OP_TEXM3x3SPEC" ,  (1, 2, (1, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXM3x3VSPEC" , (1, 1, (1, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXM3x3TEX" ,   (1, 1, (1, 0), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXDP3TEX" ,    (1, 1, (1, 2), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXM3x2DEPTH" , (1, 1, (1, 3), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXDP3" ,       (1, 1, (1, 2), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXM3x3" ,      (1, 1, (1, 2), (  1,   3)) ),
    ("VKD3D_SM1_OP_TEXDEPTH" ,     (1, 0, (1, 4), (  1,   4)) ),
    ("VKD3D_SM1_OP_BEM" ,          (1, 2, (1, 4), (  1,   4)) ),
    ("VKD3D_SM1_OP_DSX" ,          (1, 1, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_DSY" ,          (1, 1, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_TEXLDD" ,       (1, 4, (2, 1), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_SETP" ,         (1, 2) ),
    ("VKD3D_SM1_OP_TEXLDL" ,       (1, 2, (3, 0), (0xffffffff, 0xffffffff)) ),
    ("VKD3D_SM1_OP_PHASE" ,        (0, 0) ),
]

def version_ge(major1, minor1, major2, minor2):
    return (major1 > major2) or (major1 == major2 and minor1 >= minor2)

def version_le(major1, minor1, major2, minor2):
    return (major1 < major2) or (major1 == major2 and minor1 <= minor2)

def table_retrieve(table, opcode_name, major, minor):
    for name, info in table:
        if name == opcode_name:

            if len(info) >= 3:
                min_major, min_minor = info[2]
                if not version_ge(major, minor, min_major, min_minor):
                    continue
            if len(info) >= 4:
                max_major, max_minor = info[3]
                if not version_le(major, minor, max_major, max_minor):
                    continue

            return info

    return None

def indent(level):
    return "  "*level + " └─ "

# Build a number from its bytes (little endian)
def as_number(bs):
    res = 0
    for b in bs[::-1]:
        res = res << 8
        res = res | b
    return res

# Get a number stored within a range of the bits in a word (number)
def get_on_bin_range(word, ini, end):
    word = word << (32 - end)
    word = word & 0xffffffff
    word = word >> (ini + 32 - end)
    return (word, bin( (1 << (end - ini) | word) )[3:])

# Parses components within a word (number)
# If print_prefix is provided, the word is printed.
# If names are provided, returns their values in a dictionary. They are also used for formatting.
def print_word_split(word, cuts, names = {}, print_prefix = None):
    values = {}
    cuts = set(cuts)
    cuts.add(0)
    cuts.add(32)
    cuts = sorted(list(cuts))
    text = "" if print_prefix is None else print_prefix
    for ini, end in list(zip(cuts[:-1], cuts[1:]))[::-1]:
        val, bits = get_on_bin_range(word, ini, end)
        name = names[ini] if ini in names else None
        if name:
            if "?" in name:
                text += f"\033[0;37m{bits}[{val}]\033[0m "
            elif ("offset" in name) or ("string" in name) or ("start" in name):
                text += f"{bits}\033[0;33m[{name}:\033[1;33m{val}\033[0;33m]\033[0m "
            elif "char" in name:
                val = repr(chr(val))[1:-1]
                text += f"{bits}\033[0;31m[{val}]\033[0m "
            else:
                text += f"{bits}\033[0;32m[{name}:{val}]\033[0m "
            values[name] = val
        elif end - ini == 32:
            flt = struct.unpack("f", struct.pack("I", word) )[0]
            text += f"{bits}\033[0;36m[{val}|{flt}]\033[0m "
        else:
            text += f"{bits} "
    if print_prefix is not None:
        print(text)
    return values

def read_instruction_header(words, pos):
    res = print_word_split(words[pos], [0, 16, 24, 28, 29, 30, 31],
            names = {0: "opcode", 16: "flags", 24: "len", 28 : "pred", 30 : "ci", 31 : "par"}, print_prefix = indent(0))
    pos += 1
    return res, pos

def read_dst_param(words, pos, can_have_addrm = True):
    if pos >= len(words):
        print("\033[0;31m0x%08x > ERROR: PREMATURE END.\033[0m"%(4 * pos))
        return pos, False

    cuts = [0, 11, 13, 14, 16, 20, 24, 28, 31]
    names = {0: "rnum", 11: "rtype2", 16 : "wmask", 20 : "modif", 24 : "shift", 28 : "rtype1"}
    if can_have_addrm:
        names[13] = "addrm"
    res = print_word_split(words[pos], cuts, names = names, print_prefix = indent(1))
    pos += 1

    if can_have_addrm and res["addrm"]:
        if pos >= len(words):
            print("\033[0;31m0x%08x > ERROR: PREMATURE END.\033[0m"%(4 * pos))
            return pos, False

        res = print_word_split(words[pos], [0], names = {0 : "rel. addressing"}, print_prefix = indent(2))
        pos += 1

    return pos, True

def read_src_param(words, pos):
    if pos >= len(words):
        print("\033[0;31m0x%08x > ERROR: PREMATURE END.\033[0m"%(4 * pos))
        return pos, False

    cuts = [0, 11, 13, 14, 16, 24, 28, 31]
    names = {0: "rnum", 11: "rtype2", 16 : "swizzle", 24 : "modif", 28 : "rtype1"}
    names[13] = "addrm"
    res = print_word_split(words[pos], cuts, names = names, print_prefix = indent(1))
    pos += 1

    if res["addrm"]:
        if pos >= len(words):
            print("\033[0;31m0x%08x > ERROR: PREMATURE END.\033[0m"%(4 * pos))
            return pos, False

        res = print_word_split(words[pos], [0], names = {0 : "rel. addressing"}, print_prefix = indent(2))
        pos += 1

    return pos, True

# Read instructions within shader
# returns unread words on premature ending
def read_instructions(words):
    pos = 0

    if pos >= len(words):
        print("\033[0;31m0x%08x > ERROR: EXPECTED HEADER.\033[0m"%(4 * pos,))
        return words[pos:]

    print("\033[0;33m0x%08x > HEADER\033[0m"%(4 * pos,))
    res = print_word_split(words[pos], [0, 8, 16], names = {0: "minor", 8: "major", 16 : "type"}, print_prefix = indent(0))
    pos += 1
    shader_type = res["type"]
    shader_major = res["major"]
    shader_minor = res["minor"]
    shader_name = f"{shader_type}_{shader_major}_{shader_minor}"

    if shader_type == VKD3D_SM1_PS:
        shader_type = "ps"
    elif shader_type == VKD3D_SM1_VS:
        shader_type = "vs"
    else:
        return words[pos:]

    while pos < len(words):
        res = print_word_split(words[pos], [0, 16], names = {0: "opcode"})
        opcode = res["opcode"]

        if opcode not in vkd3d_sm1_opcode:
            print("\033[0;31m0x%08x > ERROR: UNKNOWN OPCODE %d.\033[0m"%(4 * pos, opcode))
            return words[pos:]

        opcode_name = vkd3d_sm1_opcode[opcode]
        print("\033[0;33m0x%08x > %s (%d)\033[0m"%(4 * pos, opcode_name, opcode))

        if opcode_name == "VKD3D_SM1_OP_COMMENT":
            res = print_word_split(words[pos], [0, 16, 27], names = {0: "opcode", 16: "size"}, print_prefix = indent(0))
            pos += 1
            pos_start = pos

            if words[pos] == as_number([ord('C'), ord('T'), ord('A'), ord('B')]):
                print_word_split(words[pos], [0, 8, 16, 24, 32],
                        names = {0 : "char0", 8 : "char1", 16 : "char2", 24: "char3"}, print_prefix = indent(1))
                pos += 1

                print_word_split(words[pos], [0, 32], names = {0: "ctab_start"}, print_prefix = indent(2))
                pos += 1

                print_word_split(words[pos], [0, 32], names = {0: "creator_offset"}, print_prefix = indent(2))
                pos += 1

                print_word_split(words[pos], [0, 8, 16, 32], names = {0: "ver_minor", 8: "ver_major", 16: "shdr_type"}, print_prefix = indent(2))
                pos += 1

                r = print_word_split(words[pos], [0, 32], names = {0: "uniforms_count"}, print_prefix = indent(2))
                uniforms_count = r["uniforms_count"]
                pos += 1

                print_word_split(words[pos], [0, 32], names = {0: "consts_offset"}, print_prefix = indent(2))
                pos += 1

                print_word_split(words[pos], [0, 32], names = {0: "flags"}, print_prefix = indent(2))
                pos += 1

                print_word_split(words[pos], [0, 32], names = {0: "tgt_string"}, print_prefix = indent(2))
                pos += 1

            for u in range(uniforms_count):
                print("%s\033[0;33m0x%08x > %s\033[0m"%(indent(2), 4 * pos, "Variable"))
                print_word_split(words[pos], [0, 32], names = {0: "name"}, print_prefix = indent(3))
                pos += 1
                print_word_split(words[pos], [0, 16, 32], names = {0: "regset", 16:"id"}, print_prefix = indent(3))
                pos += 1
                print_word_split(words[pos], [0, 32], names = {0: "regsize"}, print_prefix = indent(3))
                pos += 1
                print_word_split(words[pos], [0, 32], names = {0: "type"}, print_prefix = indent(3))
                pos += 1
                print_word_split(words[pos], [0, 32], names = {0: "default_val_offset"}, print_prefix = indent(3))
                pos += 1

            texts = ["   \033[0;31m"]
            for i in range(res["size"] - pos + pos_start):
                if pos >= len(words):
                    print("\033[0;31m0x%08x > ERROR: PREMATURE END.\033[0m"%(4 * pos))
                    return []
                res = print_word_split(words[pos], [0, 8, 16, 24],
                        names = {0 : "char0", 8 : "char1", 16 : "char2", 24: "char3"}, print_prefix = indent(1))
                texts.append(res["char0"])
                texts.append(res["char1"])
                texts.append(res["char2"])
                texts.append(res["char3"])
                pos += 1
            texts.append("\033[0m")
            texts = "".join(texts)
            print(texts)

        elif opcode_name == "VKD3D_SM1_OP_DCL":
            res, pos = read_instruction_header(words, pos)

            if pos >= len(words):
                print("\033[0;31m0x%08x > ERROR: PREMATURE END.\033[0m"%(4 * pos))
                return []
            res = print_word_split(words[pos], [0, 4, 16, 20, 27, 31],
                    names = {0: "usage", 16: "index", 27: "restype"}, print_prefix = indent(1))
            pos += 1

            pos, ok = read_dst_param(words, pos, False)
            if not ok:
                return words[pos:]

        elif opcode_name == "VKD3D_SM1_OP_DEF" or opcode_name == "VKD3D_SM1_OP_DEFI" or opcode_name == "VKD3D_SM1_OP_DEFB":
            res, pos = read_instruction_header(words, pos)

            pos, ok = read_dst_param(words, pos, True)
            if not ok:
                return words[pos:]

            count = 1 if opcode_name == "VKD3D_SM1_OP_DEFB" else 4

            for i in range(count):
                if pos >= len(words):
                    print("\033[0;31m0x%08x > ERROR: PREMATURE END.\033[0m"%(4 * pos))
                    return []
                res = print_word_split(words[pos], [0], print_prefix = indent(1))
                pos += 1

        else:
            if opcode_name == "VKD3D_SM1_OP_END":
                res, pos = read_instruction_header(words, pos)
                return words[pos:]

            elif shader_type == "ps":
                opcode_info = table_retrieve(ps_opcode_table, opcode_name, shader_major, shader_minor)

                if opcode_info is None:
                    print("\033[0;31m0x%08x > ERROR: %s (%d) IS INVALID FOR %s.\033[0m"%(4 * pos, opcode_name, opcode, shader_name))
                    return words[pos:]

            elif shader_type == "vs":
                opcode_info = table_retrieve(vs_opcode_table, opcode_name, shader_major, shader_minor)

                if opcode_info is None:
                    print("\033[0;31m0x%08x > ERROR: %s (%d) IS INVALID FOR %s.\033[0m"%(4 * pos, opcode_name, opcode, shader_name))
                    return words[pos:]

            res, pos = read_instruction_header(words, pos)

            n_dsts = opcode_info[0]
            has_predicate = (res["pred"] > 0)
            n_srcs = opcode_info[1]

            for i in range(n_dsts):
                pos, ok = read_dst_param(words, pos, True)
                if not ok:
                    return words[pos:]

            if has_predicate:
                pos, ok = read_src_param(words, pos)
                if not ok:
                    return words[pos:]

            for i in range(n_srcs):
                pos, ok = read_src_param(words, pos)
                if not ok:
                    return words[pos:]


    print("\033[0;31m0x%08x > ERROR: MISSING END.\033[0m"%(4 * pos))
    return []


if __name__ ==  "__main__":

    words = []

    with open(fname, "rb") as file:
        # Read words
        while 1:
            # Read next 4 bytes
            bs = file.read(4)
            if not bs:
                break
            words.append(as_number(bs))

    missing_words = read_instructions(words)
    for m in missing_words:
        print_word_split(m, [0], print_prefix = " ")
