# Binary d3dbc parser

Quick and dirty python script to print all the bits in a d3dbc object file in an organized way.

## Usage:

```
python3 binary-d3dbc-parser.py <file>
```

## Example output:

![Example output.](./usage.png)

## Considerations:

* All the file bits should be printed; always; in white color.

## d3dbc bit fields

![d3dbc bit fields](./sm1_d3dbc.png)
